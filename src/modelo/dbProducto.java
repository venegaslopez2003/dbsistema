/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author venegas
 */
public class dbProducto extends dbManejador implements Persistencia{
    
    public dbProducto(){
        super();
    }

    @Override
    public void insertar(Object object) throws Exception {
        productos pro =new productos();
        pro=(productos)object;
        
        String consulta ="";
        consulta="Insert into productos(codigo,nombre,fecha,precio,status)"+ "values(?,?,?,?,?)";
        
        
        if(this.conectar()){
        this.sqlConsulta=this.conexion.prepareStatement(consulta);
        this.sqlConsulta.setString(1, pro.getCodigo());
        this.sqlConsulta.setString(2, pro.getNombre());
        this.sqlConsulta.setString(3, pro.getFecha());
        this.sqlConsulta.setFloat(4, pro.getPrecio());
        this.sqlConsulta.setInt(5, pro.getStatus());
        
        this.sqlConsulta.executeUpdate();
            this.desconectar();
        }
        
        
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void actualizar(Object object) throws Exception {
       
    productos pro = (productos) object;
    
    String consulta = "UPDATE productos SET nombre = ?, fecha = ?, precio = ?, status = ? WHERE codigo = ?";
    if (this.conectar()) {
    this.sqlConsulta = this.conexion.prepareStatement(consulta);
    this.sqlConsulta.setString(1, pro.getNombre());
    this.sqlConsulta.setString(2, pro.getFecha());
    this.sqlConsulta.setFloat(3, pro.getPrecio());
    this.sqlConsulta.setInt(4, pro.getStatus());
    this.sqlConsulta.setString(5, pro.getCodigo());

    
        this.sqlConsulta.executeUpdate();
        this.desconectar();
    } else {
       
        throw new SQLException("fallo al conectar");
    }
}
    

    @Override
    public void hanilitar(Object object) throws Exception {
        
        productos pro = new productos();
        pro = (productos)object;
        
        String consulta = "";
        consulta = "update productos set status = 0 where idproducto = ?end status = 1";
        if(this.conectar()){
            this.sqlConsulta = this.conexion.prepareStatement(consulta);
            this.sqlConsulta.setInt(1, pro.getIdproductos());
            this.sqlConsulta.executeUpdate();
            this.desconectar();
    
            
            
            
            
            
        }
        
        
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

  @Override
public void deshabilitar(Object object) throws Exception {
    productos pro = (productos) object;

    String consulta = "UPDATE productos SET status = 1 WHERE idproductos = ? AND status = 0";
    if (this.conectar()) {
        this.sqlConsulta = this.conexion.prepareStatement(consulta);
        this.sqlConsulta.setInt(1, pro.getIdproductos());
        this.sqlConsulta.executeUpdate();
        this.desconectar();
    }


        
        
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean siExiste(int id) throws Exception {
        boolean exito = false;
        String consulta = "";
        consulta = "select * from productos where idproducto = ? and status =0";
        
        if(this.conectar()){
            this.sqlConsulta = this.conexion.prepareStatement(consulta);
            this.sqlConsulta.setInt(1, id);
            registros = this.sqlConsulta.executeQuery();
            if(registros.next())exito = true;
            this.desconectar();
                    
        }
        
        return exito;
        
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
   public ArrayList lista() throws Exception {
        ArrayList listaProducto = new ArrayList<productos>();
        String consulta ="select * from productos where status = 0 order by codigo";
        
        if(this.conectar()){
            this.sqlConsulta=this.conexion.prepareStatement(consulta);
            registros = this.sqlConsulta.executeQuery();
            
            while(registros.next()){
                productos pro = new productos();
                pro.setCodigo(registros.getString("codigo"));
                pro.setNombre(registros.getNString("nombre"));
                pro.setPrecio(registros.getInt("precio"));
                pro.setFecha(registros.getString("fecha"));
                pro.setIdproductos(registros.getInt("idproductos"));
                pro.setStatus(registros.getInt("status"));
                
                listaProducto.add(pro);
                
            }
        }
        this.desconectar();
        return listaProducto;
    }
   
   
    @Override
    public Object buscar(String codigo) throws Exception {
        
        productos pro = new productos();
        String consulta = "";
        consulta = "select * from productos where codigo = ?and status = 0";
        if(this.conectar()){
            this.sqlConsulta = this.conexion.prepareStatement(consulta);
            this.sqlConsulta.setString(1, codigo);
            
            registros = this.sqlConsulta.executeQuery();
            
            if(registros.next()){
                
                pro.setCodigo(codigo);
                pro.setNombre(registros.getString("nombre"));
                pro.setPrecio(registros.getInt("precio"));
                pro.setFecha(registros.getString("fecha"));
                pro.setIdproductos(registros.getInt("idproductos"));
                pro.setStatus(registros.getInt("status"));
                
                this.desconectar();
                return pro;
                
            }
            
            
            
            
        }
        
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
