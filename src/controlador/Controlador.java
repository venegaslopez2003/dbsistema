/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;
import java.awt.event.ActionEvent;
import vista.jintProductos;
import modelo.*;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;
import com.toedter.calendar.JDayChooser;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.text.ParseException;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.DefaultTableModel;
        

/**
 *
 * @author veneg
 */
public class  Controlador implements ActionListener{
    
    private jintProductos vista;
    private dbProducto db;
    private boolean EsActulizar;
    private int idProducto=0;

    public Controlador(jintProductos vista, dbProducto db) {
        this.vista = vista;
        this.db = db;
        
        
        vista.btnCancelar.addActionListener(this);
        vista.btnCerrar.addActionListener(this);
        vista.btnGuardar.addActionListener(this);
        vista.btnNuevo.addActionListener(this);
        vista.btnLimpiar.addActionListener(this);
        vista.btnBuscar.addActionListener(this);
        vista.btnDeshabilitar.addActionListener(this);
        this.deshabilitar();
        
        
        //action
        
        
        
        
        
            
        
        
    }
    
    public void limpiar() {
            vista.txtCodigo.setText("");
            vista.txtNombre.setText("");
            vista.txtPrecio.setText("");
            //vista.jdtFecha.setDate();
        }
    
    
    
    public void deshabilitar(){
    vista.txtCodigo.setEnabled(false);
      vista.txtNombre.setEnabled(false);
        vista.txtPrecio.setEnabled(false);
          vista.btnGuardar.setEnabled(false);
            vista.btnBuscar.setEnabled(false);
            vista.btnDeshabilitar.setEnabled(false);
    
        
    }
    public void cerrar (){
        int res = JOptionPane.showConfirmDialog(vista, "Desea cerrar la aplicacion?","Productos",JOptionPane.YES_OPTION,JOptionPane.QUESTION_MESSAGE);
        if (res == JOptionPane.YES_OPTION){
            vista.dispose();
        }
        
    }
        public void habilitar(){
        vista.txtCodigo.setEnabled(true);
        vista.txtNombre.setEnabled(true);
        vista.txtPrecio.setEnabled(true);
        vista.btnBuscar.setEnabled(true);
        vista.btnGuardar.setEnabled(true);
        vista.btnNuevo.setEnabled(true);
        vista.btnDeshabilitar.setEnabled(true);
        
    }
    
    
    
    
    public boolean validar(){
        boolean exito = true;
        
        if(vista.txtCodigo.getText().equals("")
               || vista.txtNombre.getText().equals("")
               || vista.txtPrecio.getText().equals("")) exito=false;
                
        return exito;
        
  
    }

    
    

   @Override
public void actionPerformed(ActionEvent ae) {
    if (ae.getSource() == vista.btnLimpiar) {
        this.limpiar();
    }
    if (ae.getSource() == vista.btnCancelar) {
        this.limpiar();
        this.deshabilitar();
    }
    if (ae.getSource() == vista.btnCerrar) {
        this.cerrar();
    }
    if (ae.getSource() == vista.btnNuevo) {
        this.habilitar();
        this.EsActulizar = false;
    }

    if (ae.getSource() == vista.btnGuardar) {
        if (this.validar()) {
            productos pro = new productos();
            pro.setCodigo(vista.txtCodigo.getText());
            pro.setNombre(vista.txtNombre.getText());
            pro.setPrecio(Float.parseFloat(vista.txtPrecio.getText()));
            pro.setStatus(0);
            pro.setFecha(this.convertirAnoMesDia(vista.jdtFecha.getDate()));
                    

            try {
                if (!this.EsActulizar) {
                    db.insertar(pro);
                    JOptionPane.showMessageDialog(vista, "Se agrego con exito el producto.");
                     this.ActualizarTabla(db.lista());
                } else {
                    pro.setIdproductos(idProducto);
                    db.actualizar(pro);
                    this.ActualizarTabla(db.lista());
                    JOptionPane.showMessageDialog(vista, "Se actualizo con exito el producto.");
                }
                this.limpiar();
                this.deshabilitar();
                this.ActualizarTabla(db.lista());
            } catch (Exception e) {
               JOptionPane.showMessageDialog(vista, "Se agrego con exito el producto.");
                try {
                    this.ActualizarTabla(db.lista());
                } catch (Exception ex) {
                    Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } else {
            JOptionPane.showMessageDialog(vista, "Falto capturar datos.");
        }
    }

    if (ae.getSource() == vista.btnBuscar) {
        productos pro = new productos();

        if (vista.txtCodigo.getText().equals("")) {
            JOptionPane.showMessageDialog(vista, "Falto capturar el codigo.");
        } else {
            try {
                pro = (productos) db.buscar(vista.txtCodigo.getText());

                if (pro.getIdproductos() != 0) {
                    idProducto = pro.getIdproductos();
                    vista.txtNombre.setText(pro.getNombre());
                    vista.txtPrecio.setText(String.valueOf(pro.getPrecio()));
                    convertirStringDate(pro.getFecha());
                    this.EsActulizar = true;
                    vista.btnGuardar.setEnabled(true);
                    vista.btnDeshabilitar.setEnabled(true);
                } else {
                    JOptionPane.showMessageDialog(vista, "Surgio un error: " );
                }
            } catch (Exception e) {
                JOptionPane.showMessageDialog(vista, "No se encontró el producto.");
               
            }
        }
    }

    if (ae.getSource() == vista.btnDeshabilitar){
        int opcion = 0;
        opcion = JOptionPane.showConfirmDialog(vista, "¿Desea deshabilitar el producto?", "Producto", 
                JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);

        if (opcion == JOptionPane.YES_OPTION) {
            productos pro = new productos();
            pro.setCodigo(vista.txtCodigo.getText());
            pro.setIdproductos(idProducto);
            try {
                this.ActualizarTabla(db.lista());
            } catch (Exception ex) {
                Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
            }

            try {
                db.deshabilitar(pro);
                JOptionPane.showMessageDialog(vista, "El producto está deshabilitado.");
                this.limpiar();
                this.deshabilitar();
                this.ActualizarTabla(db.lista());
            } catch (Exception e) {
                JOptionPane.showMessageDialog(vista, "Surgió un error: " + e.getMessage());
                
            }
        }
    }
}

public void iniciarVista() {
    vista.setTitle("Productos");
    vista.setSize(1300, 1300);
    vista.setVisible(true);
    
    try{
        this.ActualizarTabla(db.lista());
    } catch (Exception e){
         JOptionPane.showMessageDialog(vista,"Surgio Un error ");
    }
        
    }





public String convertirAnoMesDia(Date fecha){
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(fecha);
    }
    public void convertirStringDate(String fecha){
        try {
           //convertir la cadena de texto a ibjeto date
           SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
           Date date = dateFormat.parse(fecha);
           vista.jdtFecha.setDate(date);
           
        }catch(ParseException e){
            System.err.print(e.getMessage());
            
        }

    
    }
    
    public void ActualizarTabla(ArrayList<productos> arr){
        String campos[] = {"idproducto","codigo","nombre","precio","fecha"} ;
        String[][] datos = new String[arr.size()][5];
        int renglon =0;
        
        for(productos registros : arr){
            datos[renglon][0] = String.valueOf(registros.getIdproductos());
            datos[renglon][1] = registros.getCodigo();
            datos[renglon][2] = registros.getNombre();
            
            datos[renglon][3] = String .valueOf(registros.getPrecio());
            datos[renglon][4] = registros.getFecha();
            
            
            renglon++;
        }
        DefaultTableModel tb = new DefaultTableModel(datos,campos);
        vista.lista.setModel(tb);
        
        
    }
    
    
    
}

  